% Generate number

generate_number(Min, Max, Min) :-
    Min =< Max.

generate_number(Min, Max, N) :-
   NewMin is Min+1, 
   NewMin =< Max, 
   generate_number(NewMin, Max, N).


% Variables

variables([a1, a2, a3, b1, b2, b3]).


% Choose var index

choose_var_index(LastUsed, LastLocal, _, LastLocal, 1, Index) :-
    generate_number(1, LastUsed, Index).

choose_var_index(LastUsed, LastLocal, FlagIn, LastLocal, FlagIn, Index) :-
    NewLastUsed is LastUsed+1,
    generate_number(NewLastUsed, LastLocal, Index).

choose_var_index(_, LastLocal, FlagIn, Index, FlagIn, Index) :-
    Index is LastLocal+1.


% Take var

take_var(1, [X|_], X).

take_var(Index, [_|RestVars], Var) :-
    Index > 1,
    NewIndex is Index-1,
    take_var(NewIndex, RestVars, Var).


% Insert arg

insert_arg(LastUsed, LastLocal, FlagIn, Arg, RetLastLocal, FlagOut) :-
    variables(L),
    choose_var_index(LastUsed, LastLocal, FlagIn, RetLastLocal, FlagOut, Index),
    take_var(Index, L, Arg).

% Usage:
% insert_arg(3, 4, 0, Arg, RetLastLocal, FlagOut).


% Build arg list

build_arg_list(1, vars(LastUsed, LastLocal), 0, [Arg|[]], RetLastLocal) :-
    insert_arg(LastUsed, LastLocal, 0, Arg, RetLastLocal, 1).

build_arg_list(1, vars(LastUsed, LastLocal), 1, [Arg|[]], RetLastLocal) :-
    insert_arg(LastUsed, LastLocal, 1, Arg, RetLastLocal, _).

build_arg_list(N, vars(LastUsed, LastLocal), Flag, [Arg|RestArg], RetLastUsed) :-
    N > 1,
    insert_arg(LastUsed, LastLocal, Flag, Arg, RetLastLocal, FlagOut),
    NewN is N-1,
    build_arg_list(NewN, vars(LastUsed, RetLastLocal), FlagOut, RestArg, RetLastUsed).

% Usage:
% build_arg_list(2, vars(1,3), 0, List, RetLastLocal).


match_args(Arg1, Arg2, [], [b(Arg1, Arg2)]).

match_args(Arg1, Arg2, [b(Arg1, Arg2)|Rest], [b(Arg1, Arg2)|Rest]).

match_args(Arg1, Arg2, [b(Var, Val)|RestBindings], [b(Var, Val)|ResultBindings]) :-
    Arg1 \= Var,
    Arg2 \= Val,
    match_args(Arg1, Arg2, RestBindings, ResultBindings).


match_arg_lists([], [], Bindings, Bindings).

match_arg_lists([Arg1|RestArgs1], [Arg2|RestArgs2], BindingsIn, BindingsOut) :-
    match_args(Arg1, Arg2, BindingsIn, Bindings1),
    match_arg_list(RestArgs1, RestArgs2, Bindings1, BindingsOut).

    
