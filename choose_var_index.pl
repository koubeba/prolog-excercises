choose_var_index(Flag, LastUsed, LastLocal, Index) :-
	variables(V),
	length(V, L),
	generate_number(Index, 1, L),
       	set_flag(Flag, LastUsed, LastLocal, Index).	

increment_last_local(Index, LastLocal) :-
	Index =< LastLocal.

increment_last_local(Index, LastLocal) :-
	Index > LastLocal,
	LastLocal is LastLocal + 1.

set_flag(Flag, LastUsed, LastLocal, Index) :-
	Index =< LastUsed,
	Flag is 1.

set_flag(Flag, LastUsed, LastLocal, Index) :-
	Index > LastUsed.
