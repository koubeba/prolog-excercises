generate_number(Start, Start, Koniec).

generate_number(Liczba, Start, Koniec) :-
	NowyStart is Start+1,
	NowyStart =< Koniec,
	generate_number(Liczba, NowyStart, Koniec).
